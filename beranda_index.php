<h1 class="text-center p-3 mb-2 bg-dark text-white">Penerimaan Peserta Didik Baru Tahun Ajaran 2023/2024 SMK Negeri 1 Elite</h1>
<div class="p-3 mb-2 bg-secondary text-black">
<h4>A. VISI SMK NEGERI 1 ELITE</h4>
<h5>Terwujudnya Pendidikan Vokasi yang menghasilkan lulusan berkarakter profil pelajar pancasila dan kompeten</h5>


<h4>B. MISI SMK NEGERI 1 ELITE</h4>
<h5>1. Memberikan pendidikan yang berkualitas dengan mengutamakan pembentukan karakter Profil Pelajar Pancasila.</h5>
<h5>2. Membekali peserta didik dengan pengetahuan, keterampilan dan teknologi sesuai dengan tantangan global.</h5>


<h4>C. TUJUAN SMK NEGERI 1 ELITE</h4>
<h5>1. Membentuk sikap,perilaku, perbuatan yang beriman dan bertakwa kepada Tuhan YME serta Berakhlak Mulia</h5>
<h5>2. Membentuk sikap,perilaku dan perbuatan yang Berkebhinekaan Global</h5>
<h5>3. Membentuk sikap dan perilaku Gotong Royong</h5>
<h5>4. Membentuk pribadi yang Mandiri, Kreatif, Inovatif dan bernalar Kritis</h5>
<h5>5. Membentuk pribadi yang terampil dan kompeten sesuai bidangnya</h5>
</div>

<div class="text-center p-3 mb-2 bg-secondary text-black">
    <h2>Daftar Jurusan</h2>
    <h5>1. Teknik Komputer Dan Jaringan</h5>
    <h5>2. Teknik Instalasi Tenaga Listrik</h5>
    <h5>3. Teknik Kendaraan Ringan Otomotif</h5>
    <h5>4. Teknik Dan Bisnis Sepeda Motor</h5>
    <h5>5. Akuntansi Dan Keuangan Lembaga</h5>
    <h5>6. Teknik Permesinan</h5>
</div>
