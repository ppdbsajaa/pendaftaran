<?php   
session_start();
if(empty($_SESSION['username'])){
    header("location:../indexx.php?page=admin");
}else{
    //echo $_SESSION['username'];
 //Koneksi Database
 $server ="localhost";
 $user ="root";
 $pass ="";
 $database ="dblatihan";

 $koneksi = mysqli_connect($server, $user, $pass, $database)or die(mysqli_error($koneksi));

 //jika tombol simpan diklik
 if(isset($_POST['bsimpan']))
 {
     //pengujian apakah data akan diedit / disimpan baru
     if($_GET['hal'] == "edit")
     {
         //data akan diedit
         $edit = mysqli_query($koneksi, "UPDATE tmhs set
                                             nama = '$_POST[tnama]',
                                             alamat = '$_POST[talamat]',
                                             nisn = '$_POST[tnisn]',
                                             sekolah = '$_POST[tsekolah]',
                                             lahir = '$_POST[tlahir]',
                                             kelamin = '$_POST[tkelamin]',
                                             agama = '$_POST[tagama]'
                                          WHERE id_mhs = '$_GET[id]'
                                         ");
         if($edit) //jika edit suksess
         {
             echo "<script>
                 alert('edit data suksess!');
                 document.location='halaman_admin.php';
             </script>";
         }
         else
         {
             echo "<script>
                 alert('edit data GAGAL!!');
                 document.location='halaman_admin.php';
             </script>";
         }
     }
     else
     {
         //Data akan disimpan baru
         $simpan = mysqli_query($koneksi, "INSERT INTO tmhs (nama, alamat, nisn, sekolah, lahir, kelamin, agama)
                                           VALUES  ('$_POST[tnama]',
                                                    '$_POST[talamat]',
                                                    '$_POST[tnisn]',
                                                    '$_POST[tsekolah]',
                                                    '$_POST[tlahir]',
                                                    '$_POST[tkelamin]',
                                                    '$_POST[tagama]')
                                         ");
         if($simpan) //jika simpan suksess
         {
             echo "<script>
                 alert('simpan data suksess!');
                 document.location='halaman_admin.php';
             </script>";
         }
         else
         {
             echo "<script>
                 alert('simpan data GAGAL!!');
                 document.location='halaman_admin.php';
             </script>";
         }
     }        


     
 }

 //pengujian jika tombol edit / hapus di klik
 if(isset($_GET['hal']))
 {
     //pengujian jika edit data 
     if($_GET['hal'] == "edit")
     {
         //tampilkan data yang akan diedit
         $tampil = mysqli_query($koneksi, "SELECT * FROM tmhs WHERE id_mhs = '$_GET[id]' ");
         $data = mysqli_fetch_array($tampil);
         if($data)
         {
             //jika data ditemukan, maka data akan ditampung kedalam variabel
             $vnama = $data['nama'];
             $valamat = $data['alamat'];
             $vnisn = $data['nisn'];
             $vsekolah = $data['sekolah'];
             $vlahir = $data['lahir'];
             $vkelamin = $data['kelamin'];
             $vagama = $data['agama'];
         }
     }
     else if ($_GET['hal'] == "hapus")
     {
         //persiapan hapus data
         $hapus = mysqli_query($koneksi, "DELETE FROM tmhs WHERE id_mhs = '$_GET[id]'");
         if($hapus){
             echo "<script>
                 alert('hapus data suksess!');
                 document.location='halaman_admin.php';
             </script>";
         }
     }
 }

?>


<!DOCTYPE html>
<html>
<head>
 <title>Halaman admin pendaftaran</title>
 <link rel="stylesheet" type="text/css" href="admin_bootstrap.min.css">
</head>
<body>
<div class="container">
 <h2 class="text-center">BIODATA SISWA SISWI</h2>    

 <!-- awal card from -->
 <div class="card">
 <div class="card-header bg-primary text-white">
     Form Input Data Siswa Siswi
 </div>
 <div class="card-body">
     <form method="post" action="">
         <div class="form-group">
             <label>Nama</label>
             <input type="text" name="tnama" value="<?=@$vnama?>" class="form-control" placeholder="Input Nama anda disini!" required>
         </div>
         <div class="form-group">
             <label>Alamat</label>
             <textarea class="form-control" name="talamat" placeholder="Input Alamat anda disini!"><?=@$valamat?></textarea>
         </div>
         <div class="form-group">
             <label>Nisn</label>
             <input type="text" name="tnisn" value="<?=@$vnisn?>" class="form-control" placeholder="Input Nisn anda disini!" required>
         </div>
         <div class="form-group">
             <label>Sekolah</label>
             <input type="text" name="tsekolah" value="<?=@$vsekolah?>" class="form-control" placeholder="Input Sekolah anda disini!" required>
         </div>
         <div class="form-group">
             <label>Tanggal Lahir</label>
             <input type="text" name="tlahir" value="<?=@$vlahir?>" class="form-control" placeholder="Input Tanggal Lahir anda disini!" required>
         </div>
         <div class="form-group">
             <label>Jenis Kelamin</label>
             <select class="form-control" name="tkelamin">
                 <option value="<?=@$vkelamin?>"><?=@$vkelamin?></option>
                 <option value="Perempuan">Perempuan</option>
                 <option value="Laki-laki">Laki-laki</option>
             </select>
         </div>
         <div class="form-group">
             <label>Agama</label>
             <select class="form-control" name="tagama">
                 <option value="<?=@$vagama?>"><?=@$vagama?></option>
                 <option value="Kristen">Kristen</option>
                 <option value="Katolik">Katolik</option>
                 <option value="Konghucu">Konghucu</option>
                 <option value="Islam">Islam</option>
                 <option value="Budha">Budha</option>
                 <option value="Hindu">Hindu</option>
             </select>
         </div>

         <button type="submit" class="btn btn-success" name="bsimpan">Simpan</button>
         <button type="reset" class="btn btn-danger" name="breset">Kosongkan</button>

     </form>
 </div>
 </div>
 <!-- akhir card from -->

 <!-- awal card tabel -->
     <div class="card">
 <div class="card-header bg-success text-white">
     Daftar Siswa Siswi
 </div>
 <div class="card-body">
     
     <table class="table table-bordered table-striped">
         <tr>
             <th>No.</th>
             <th>Nama</th>
             <th>Alamat</th>
             <th>Nisn</th>
             <th>Sekolah</th>
             <th>Tanggal Lahir</th>
             <th>Jenis Kelamin</th>
             <th>Agama</th>
             <th>Aksi</th>
         </tr>
         <?php
             $no = 1;
             $tampil = mysqli_query($koneksi, "SELECT * from tmhs order by id_mhs desc");
             while($data = mysqli_fetch_array($tampil)) :
         
         ?>
         <tr>
             <td><?=$no++;?></td>
             <td><?=$data['nama']?></td>
             <td><?=$data['alamat']?></td>
             <td><?=$data['nisn']?></td>
             <td><?=$data['sekolah']?></td>
             <td><?=$data['lahir']?></td>
             <td><?=$data['kelamin']?></td>
             <td><?=$data['agama']?></td>
             <td>
                 <a href="halaman_admin.php?hal=edit&id=<?=$data['id_mhs']?>" class="btn btn-warning"> Edit </a>
                 <a href="halaman_admin.php?hal=hapus&id=<?=$data['id_mhs']?>" on click="return confirm('Apakah yakin ingin menghapus data ini?')" class="btn btn-danger"> Hapus </a>
             </td>
         </tr>
         <?php endwhile; //penutup perulangan while ?>
     </table>

 </div>
 </div>
 <!-- akhir card tabel -->

</div>

<div class="container mt-4">
<a href="logout.php" on class="btn btn-secondary">LOGOUT</a>

<script type="text/javascript" src="admin_bootstrap.min.js"></script>
</body>
</html>
<?php } ?>
   