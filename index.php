<!doctype html>
<html lang="en">
  <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--Bootstrap CSS-->
     <link rel="stylesheet" href="bootstrap.min.css">
    <title>Pendaftaran Siswa Baru</title>
    </head>
  <body>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
              <div class="container">
              <a class="navbar-brand" href="#">SMKN 1 ELITE</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
           <ul class="navbar-nav">
           <li class="nav-item active">
                    <a class="nav-link" href="beranda_index.php">Beranda</a>
           <li class="nav-item active">
                    <a class="nav-link" href="halaman_siswa.php">siswa</a>
           <li class="nav-item active">
                    <a class="nav-link" href="index_admin.php">admin</a>
          <li class="nav-item active">
                    <a class="nav-link" href="pengumuman_login.php">pengumuman</a>
            </nav>
<!-- ini adalah konten -->
<div class="container mt-4">
        
        <?php
        if(isset($_GET['page'])) {
            $page = $_GET['page'];
    
            if($page == 'beranda') {
                include "beranda_index.php";
            } elseif($page =='siswa') {
                include "halaman_siswa.php";
            } elseif($page =='admin') {
                include "index_admin.php";
            } elseif($page =='pengumuman') {
                include "pengumuman2.php";
            }
        }else{
            include "beranda_index.php";
        }
        ?>

<h1 class="text-center p-3 mb-2 bg-dark text-white">Penerimaan Peserta Didik Baru Tahun Ajaran 2023/2024 SMK Negeri 1 Elite</h1>
<div class="p-3 mb-2 bg-secondary text-black">
<h4>A. VISI SMK NEGERI 1 ELITE</h4>
<h5>Terwujudnya Pendidikan Vokasi yang menghasilkan lulusan berkarakter profil pelajar pancasila dan kompeten</h5>


<h4>B. MISI SMK NEGERI 1 ELITE</h4>
<h5>1. Memberikan pendidikan yang berkualitas dengan mengutamakan pembentukan karakter Profil Pelajar Pancasila.</h5>
<h5>2. Membekali peserta didik dengan pengetahuan, keterampilan dan teknologi sesuai dengan tantangan global.</h5>


<h4>C. TUJUAN SMK NEGERI 1 ELITE</h4>
<h5>1. Membentuk sikap,perilaku, perbuatan yang beriman dan bertakwa kepada Tuhan YME serta Berakhlak Mulia</h5>
<h5>2. Membentuk sikap,perilaku dan perbuatan yang Berkebhinekaan Global</h5>
<h5>3. Membentuk sikap dan perilaku Gotong Royong</h5>
<h5>4. Membentuk pribadi yang Mandiri, Kreatif, Inovatif dan bernalar Kritis</h5>
<h5>5. Membentuk pribadi yang terampil dan kompeten sesuai bidangnya</h5>
</div>

<div class="text-center p-3 mb-2 bg-secondary text-black">
    <h2>Daftar Jurusan</h2>
    <h5>1. Teknik Komputer Dan Jaringan</h5>
    <h5>2. Teknik Instalasi Tenaga Listrik</h5>
    <h5>3. Teknik Kendaraan Ringan Otomotif</h5>
    <h5>4. Teknik Dan Bisnis Sepeda Motor</h5>
    <h5>5. Akuntansi Dan Keuangan Lembaga</h5>
    <h5>6. Teknik Permesinan</h5>
</div>

</html>