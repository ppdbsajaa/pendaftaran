<?php   
    //Koneksi Database
    $server ="localhost";
    $user ="root";
    $pass ="";
    $database ="dblatihan";

    $koneksi = mysqli_connect($server, $user, $pass, $database)or die(mysqli_error($koneksi));

    //jika tombol simpan diklik
    if(isset($_POST['bsimpan']))
    {
        //Data akan disimpan baru
        $simpan = mysqli_query($koneksi, "INSERT INTO tmhs (nama, alamat, nisn, sekolah, lahir, kelamin, agama)
                                          VALUES  ('$_POST[tnama]',
                                                   '$_POST[talamat]',
                                                   '$_POST[tnisn]',
                                                   '$_POST[tsekolah]',
                                                   '$_POST[tlahir]',
                                                   '$_POST[tkelamin]',
                                                   '$_POST[tagama]')
                                        ");
        if($simpan) //jika simpan suksess
        {
            echo "<script>
                alert('simpan data suksess!');
                document.location='siswa.php';
            </script>";
        }
        else
        {
            echo "<script>
                alert('simpan data GAGAL!!');
                document.location='siswa.php';
            </script>";
        }        
    }

?>



<!DOCTYPE html>
<html>
<head>
    <title>Halaman Pendaftaran</title>
    <link rel="stylesheet" type="text/css" href="siswa_bootstrap.min.css">
</head>
<body>


    <!-- awal card tabel -->
        <div class="card">
    <div class="card-header bg-success text-white">
        Daftar Siswa Siswi
    </div>
    <div class="card-body">
        
        <table class="table table-bordered table-striped">
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Nisn</th>
                <th>Sekolah</th>
                <th>Tanggal Lahir</th>
                <th>Jenis Kelamin</th>
                <th>Agama</th>
            </tr>
            <?php
                $no = 1;
                $tampil = mysqli_query($koneksi, "SELECT * from tmhs order by id_mhs desc");
                while($data = mysqli_fetch_array($tampil)) :
            
            ?>
            <tr>
                <td><?=$no++;?></td>
                <td><?=$data['nama']?></td>
                <td><?=$data['alamat']?></td>
                <td><?=$data['nisn']?></td>
                <td><?=$data['sekolah']?></td>
                <td><?=$data['lahir']?></td>
                <td><?=$data['kelamin']?></td>
                <td><?=$data['agama']?></td>
            </tr>
            <?php endwhile; //penutup perulangan while ?>
        </table>

    </div>
    </div>
    <!-- akhir card tabel -->

</div>

<div class="container mt-4">
<a href="logout.php" on class="btn btn-danger">LOGOUT</a>

<script type="text/javascript" src="siswa_bootstrap.min.js"></script>
</body>
</html>