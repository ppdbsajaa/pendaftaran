<?php   
    //Koneksi Database
    $server ="sql206.infinityfree.com";
    $user ="if0_34477345";
    $pass ="MmSRTVuuVQl296e";
    $database ="if0_34477345_smkn1elite";

    //buat koneksi
    $koneksi = mysqli_connect($server, $user, $pass, $database) or die(mysqli_error($koneksi));

    //jika tombol simpan diklik
    if(isset($_POST['bsimpan'])) {

        //pengujian apakah data akan diedit / disimpan baru
        if(isset($_GET['hal']) == "edit") {
            //data akan di edit
            $edit = mysqli_query($koneksi, "UPDATE pengumuman SET
                                                pengumuman = '$_POST[tpengumuman]'
                                             WHERE id= '$_GET[id]'
                                            ");

            //jika edit suksess
            if($edit) {
                echo "<script>
                    alert('edit data suksess!');
                    document.location='pengumuman2.php';
                </script>";
            } else {
                echo "<script>
                    alert('edit data GAGAL!!');
                    document.location='pengumuman2.php';
                </script>";
            }
        }else{
        
            //Data akan disimpan baru
            $simpan = mysqli_query($koneksi, " INSERT INTO pengumuman (pengumuman)
                                              VALUE  ( '$_POST[tpengumuman]' )
                                            ");

            if($simpan) //jika simpan suksess
            {
                echo "<script>
                    alert('simpan data suksess!');
                    document.location='pengumuman2.php';
                </script>";
            }
            else
            {
                echo "<script>
                    alert('simpan data GAGAL!!');
                    document.location='pengumuman2.php';
                </script>";
            }
        }        
    }    

//deklarasi variabel untuk menampung data yang akan diedit
$vpengumuman = "";    

    //pengujian jika tombol edit / hapus di klik
    if(isset($_GET['hal'])) {

        //pengujian jika edit data 
        if($_GET['hal'] == "edit") {

            //tampilkan data yang akan diedit
            $tampil = mysqli_query($koneksi, "SELECT * FROM pengumuman WHERE id= '$_GET[id]' ");
            $data = mysqli_fetch_array($tampil);
            if($data) {
                //jika data ditemukan, maka data akan ditampung kedalam variabel
                $vpengumuman = $data['pengumuman'];
            }
        }
        else if ($_GET['hal'] == "hapus")
        {
            //persiapan hapus data
            $hapus = mysqli_query($koneksi, "DELETE FROM pengumuman WHERE id = '$_GET[id]'");
            if($hapus){
                echo "<script>
                    alert('hapus data suksess!');
                    document.location='pengumuman2.php';
                </script>";
            }
            else
            {
                echo "<script>
                    alert('hapus data GAGAL!!');
                    document.location='pengumuman2.php';
                </script>";
            }
        }
    }

?>


<!DOCTYPE html>
<html>
<head>
	<title>Halaman pengumuman</title>
	<link rel="stylesheet" type="text/css" href="pengumuman_bootstrap.min.css">
</head>
<body>
<div class="container">
    <h2 class="text-center">PENGUMUMAN</h2>    

    <!-- awal card from -->
    <div class="card">
    <div class="card-header bg-primary text-white">
        Form Pengumuman Siswa
    </div>
    <div class="card-body">
        <form method="post" action="">
            <div class="form-group">
                <label>pengumuman</label>
                <input type="text" name="tpengumuman" value="<?=$vpengumuman?>" class="form-control" placeholder="Input pengumuman anda disini!" required>
            </div>

            <button type="submit" class="btn btn-success" name="bsimpan">Simpan</button>
            <button type="reset" class="btn btn-danger" name="breset">Kosongkan</button>

        </form>
    </div>
    </div>
    <!-- akhir card from -->

    <!-- awal card tabel -->
        <div class="card">
    <div class="card-header bg-success text-white">
        Pengumuman SMK Negeri 1 Purwoasri
    </div>
    <div class="card-body">
        
        <table class="table table-bordered table-striped">
            <tr>
                <th>No.</th>
                <th>Pengumuman</th>
                <th>Aksi</th>
            </tr>
            <?php
                $no = 1;
                $tampil = mysqli_query($koneksi, "SELECT * from pengumuman order by id desc");
                while($data = mysqli_fetch_array($tampil)) :
            
            ?>
            <tr>
                <td><?=$no++ ?></td>
                <td><?=$data['pengumuman']?></td>
                <td>
                    <a href="pengumuman2.php?hal=edit&id=<?=$data['id']?>" class="btn btn-warning"> Edit </a>
                    <a href="pengumuman2.php?hal=hapus&id=<?= $data['id'] ?>" class="btn btn-danger" onclick="return confirm('Apakah yakin ingin menghapus data ini?')" > Hapus </a>
                </td>
            </tr>
            <?php endwhile; //penutup perulangan while ?>
        </table>

    </div>
    </div>
    <!-- akhir card tabel -->

</div>

<div class="container mt-4">
<a href="logout.php" on class="btn btn-secondary">LOGOUT</a>

<script type="text/javascript" src="pengumuma_bootstrap.min.js"></script>
</body>
</html>