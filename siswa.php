<?php   
    //Koneksi Database
    $server ="localhost";
    $user ="root";
    $pass ="";
    $database ="dblatihan";

    $koneksi = mysqli_connect($server, $user, $pass, $database)or die(mysqli_error($koneksi));

    //jika tombol simpan diklik
    if(isset($_POST['bsimpan']))
    {
        //Data akan disimpan baru
        $simpan = mysqli_query($koneksi, "INSERT INTO tmhs (nama, alamat, nisn, sekolah, lahir, kelamin, agama)
                                          VALUES  ('$_POST[tnama]',
                                                   '$_POST[talamat]',
                                                   '$_POST[tnisn]',
                                                   '$_POST[tsekolah]',
                                                   '$_POST[tlahir]',
                                                   '$_POST[tkelamin]',
                                                   '$_POST[tagama]')
                                        ");
        if($simpan) //jika simpan suksess
        {
            echo "<script>
                alert('simpan data suksess!');
                document.location='siswa.php';
            </script>";
        }
        else
        {
            echo "<script>
                alert('simpan data GAGAL!!');
                document.location='siswa.php';
            </script>";
        }        
    }

?>



<!DOCTYPE html>
<html>
<head>
    <title>Halaman Pendaftaran</title>
    <link rel="stylesheet" type="text/css" href="siswa_bootstrap.min.css">
</head>
<body>
<div class="container">
    <h2 class="text-center">BIODATA SISWA SISWI</h2>    

    <!-- awal card from -->
    <div class="card">
    <div class="card-header bg-primary text-white">
        Form Input Data Siswa Siswi
    </div>
    <div class="card-body">
        <form method="post" action="">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="tnama" class="form-control" placeholder="Input Nama anda disini!" required>
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <textarea class="form-control" name="talamat" placeholder="Input Alamat anda disini!"></textarea>
            </div>
            <div class="form-group">
                <label>Nisn</label>
                <input type="text" name="tnisn" class="form-control" placeholder="Input Nisn anda disini!" required>
            </div>
            <div class="form-group">
                <label>Sekolah</label>
                <input type="text" name="tsekolah" class="form-control" placeholder="Input Sekolah anda disini!" required>
            </div>
            <div class="form-group">
                <label>Tanggal Lahir</label>
                <input type="text" name="tlahir" class="form-control" placeholder="Input Tanggal Lahir anda disini!" required>
            </div>
            <div class="form-group">
                <label>Jenis Kelamin</label>
                <select class="form-control" name="tkelamin">
                    <option></option>
                    <option value="Perempuan">Perempuan</option>
                    <option value="Laki-laki">Laki-laki</option>
                </select>
            </div>
            <div class="form-group">
                <label>Agama</label>
                <select class="form-control" name="tagama">
                    <option></option>
                    <option value="Kristen">Kristen</option>
                    <option value="Katolik">Katolik</option>
                    <option value="Konghucu">Konghucu</option>
                    <option value="Islam">Islam</option>
                    <option value="Budha">Budha</option>
                    <option value="Hindu">Hindu</option>
                </select>
            </div>

            <button type="submit" class="btn btn-success" name="bsimpan">Simpan</button>
            <button type="reset" class="btn btn-danger" name="breset">Kosongkan</button>

        </form>
    </div>
    </div>
    <!-- akhir card from -->


<div class="container mt-4">
<a href="logout.php" on class="btn btn-danger">LOGOUT</a>
<a href="pengumuman_siswa.php" on class="btn btn-danger">PENGUMUMAN</a>
<a href="jumlah_pendaftar.php" on class="btn btn-danger">JUMLAH PENDAFTAR</a>

<script type="text/javascript" src="siswa_bootstrap.min.js"></script>
</body>
</html>